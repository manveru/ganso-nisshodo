{ euphenix }:
let
  inherit (euphenix.lib) take optionalString hasPrefix nameValuePair;
  inherit (euphenix) build loadPosts sortByRecent mkPostCSS cssTag;

  withLayout = body: [ ./templates/layout.html body ];

  vars = rec {
    activeClass = route: prefix:
      optionalString (hasPrefix prefix route) "active";
    liveJS = ''<script src="/js/live.js"></script>'';
    css = cssTag (mkPostCSS ./css);
  };

  route = template: title: id: {
    template = withLayout template;
    variables = vars // { inherit id title; };
  };

  mkRSS = feed:
    euphenix.mkDerivation {
      name = "rss";
      buildInputs = [ euphenix.ruby euphenix.coreutils ];
      buildCommand = ''
        mkdir -p $out
        ruby ${./nix/rss.rb} ${__toFile "rss.json" (__toJSON feed)} > $out/feed
      '';
    };

  pageRoutes = {
    "/index.html" = route ./templates/home.html "Home" "home";
    "/about.html" = route ./templates/about.html "About" "about";
  };

in build {
  src = ./.;
  routes = pageRoutes;
  # favicon = ./static/images/favicon.svg;
}
